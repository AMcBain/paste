package models;

import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

import org.joda.time.DateTime;

import play.db.jpa.GenericModel;

@Entity
public class Paste extends GenericModel
{
    private static final int MAX_SIZE = 100000;

    private static final Pattern CHARSET = Pattern.compile("charset=[\"']?([^'\"]*)");
    private static final Pattern CHARCHK = Pattern.compile("[-\\w]+");
    private static final Pattern XHMLDOC = Pattern.compile("(?s)(?i)^.*<!doctype[\\s\\n]+html[\\s\\n]+public[\\s\\n]+\"-//w3c//dtd xhtml");

    @Id
    public String id;

    public Date updated;

    @Column(nullable=false)
    public String contentType = "text/html; charset=";

    public String encoding;

    // "Nobody will need more than 100,000 characters."
    @Column(length=MAX_SIZE, nullable=false)
    public String html = "";

    @Column(length=MAX_SIZE, nullable=false)
    public String css = "";

    @Column(length=MAX_SIZE, nullable=false)
    public String js = "";

    public void update (String html, String css, String js)
    {
        updated = new Date();
        this.html = html.substring(0, Math.min(html.length(), Paste.MAX_SIZE));
        this.css = css.substring(0, Math.min(css.length(), Paste.MAX_SIZE));
        this.js = js.substring(0, Math.min(js.length(), Paste.MAX_SIZE));

        // Try and identify the encoding and doctype.
        String snippet = html.substring(0, Math.min(html.length(), 1024));
        Matcher charset = CHARSET.matcher(snippet);

        if (charset.find() && charset.groupCount() > 0 && CHARCHK.matcher(charset.group(1)).matches())
        {
            encoding = charset.group(1).replace("\n", "");
        }

        if (XHMLDOC.matcher(snippet).matches())
        {
            contentType = "application/xhtml+xml; charset=";
        }
        else
        {
            contentType = "text/html; charset=";
        }
    }

    public String getId ()
    {
        return id;
    }

    public DateTime getExpiration ()
    {
        return new DateTime(updated).plusDays(7);
    }

    @Override
    public Object _key ()
    {
        return getId();
    }
}

package controllers;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.regex.Pattern;

import javax.persistence.PersistenceException;

import models.Paste;

import play.Logger;
import play.Play;
import play.mvc.Controller;
import play.mvc.Router;

public class Application extends Controller
{
    private static final Pattern HEAD_END = Pattern.compile("(</head>)", Pattern.CASE_INSENSITIVE);
    private static final Pattern BODY_END = Pattern.compile("(</body>)", Pattern.CASE_INSENSITIVE);

    private static final String DEFAULT_HTML;

    static {
        String html = "";
        try
        {
            html = new String(Files.readAllBytes(Play.getFile("conf/html").toPath()), StandardCharsets.UTF_8);
        }
        catch (IOException e)
        {
            Logger.log4j.warn("No 'config/html' file.");
        }
        DEFAULT_HTML = html;
    }

    private static Paste create (String html, String css, String js)
    {
        Paste paste = new Paste();
        paste.update(html, css, js);

        // Math.pow(22, 8) -> huge number (as UUID generates permutations). So
        // this could loop forever, but with a clear-out job running once a day
        // ... very unlikely. Of course what with Murphy and his law, it seems
        // possible I will eventually be eating these words. Got any seasoning?
        boolean error;
        do
        {
            try
            {
                error = false;
                paste.id = UUID.randomUUID().toString().substring(0, 8);
                paste.save();
            }
            catch (PersistenceException e)
            {
                error = true;
            }
        } while (error);

        return paste;
    }

    public static void create ()
    {
        Paste paste = create(DEFAULT_HTML, "", "");
        edit(paste.id);
    }

    public static void edit (String id)
    {
        Paste paste = Paste.findById(id);
        notFoundIfNull(paste, id);
        render(paste);
    }

    public static void save (String id, String html, String css, String js)
    {
        boolean magic = request.headers.containsKey("x-magic");

        if (!magic)
        {
            checkAuthenticity();
        }

        Paste paste = Paste.findById(id);
        notFoundIfNull(paste, id);
        paste.update(html, css, js);
        paste.save();

        if (magic)
        {
            ok();
        }
        edit(id);
    }

    public static void view (String id)
    {
        Paste paste = Paste.findById(id);
        notFoundIfNull(paste, id);

        Map map = Collections.singletonMap("id", id);
        String styles = "    <link rel='stylesheet' href='" + Router.reverse("Application.styles", map).url + "'>";
        String script = "    <script src='" + Router.reverse("Application.script", map).url + "'></script>";
        String html = paste.html;

        if (!paste.css.isEmpty())
        {
            if (paste.css.startsWith("/*LESS*/"))
            {
                styles = styles.replace("stylesheet", "stylesheet/less") +
                        "\n        <script>less = {env: 'development', dumpLineNumbers: 'comments'};</script>" +
                        "\n        <script src='" + Router.reverse(Play.getVirtualFile("public/javascripts/lib/less.min.js")) + "'></script>";
            }
            html = HEAD_END.matcher(html).replaceFirst(styles + "\n    $1");
        }

        if (!paste.js.isEmpty())
        {
            html = HEAD_END.matcher(html).replaceFirst(script + "\n    $1");
        }

        List<String> agent = request.headers.get("user-agent").values;
        boolean validator = (agent.size() > 0 ? agent.get(0) : "").contains("http://validator.w3.org/services");

        if ((request.querystring == null || request.querystring.isEmpty()) && !validator)
        {
            String toolbar = "    <iframe src='" + Router.reverse("Application.toolbar", map).url +
                    "' style='position:fixed;top:0;right:0;width:33px;height:153px;border:none'></iframe>\n";

            if (BODY_END.matcher(html).find())
            {
                html = BODY_END.matcher(html).replaceFirst(toolbar + "    $1");
            }
            else
            {
                html += toolbar;
            }
        }

        response.setHeader("Cache-Control", "no-cache");
        response.setHeader("X-Robots-Tag", "noarchive");
        response.setHeader("X-Robots-Tag", "nofollow");
        response.setHeader("X-Robots-Tag", "unavailable_after: " + paste.getExpiration().toString("dd-MMM-yy HH:mm:ss z"));
        response.contentType = paste.contentType + (paste.encoding == null ? response.encoding : paste.encoding);
        renderText(html);
    }

    public static void fork (String id)
    {
        Paste paste = Paste.findById(id);
        notFoundIfNull(paste, id);
        Paste fork = create(paste.html, paste.css, paste.js);
        edit(fork.id);
    }

    public static void styles (String id)
    {
        Paste paste = Paste.findById(id);
        notFoundIfNull(paste, id);
        response.contentType = "text/css; charset=" + response.encoding;
        renderText(paste.css);
    }

    public static void script (String id)
    {
        Paste paste = Paste.findById(id);
        notFoundIfNull(paste, id);
        response.contentType = "text/css; charset=" + response.encoding;
        renderText(paste.js);
    }

    public static void toolbar (String id)
    {
        Paste paste = Paste.findById(id);
        notFoundIfNull(paste, id);
        render(paste);
    }
}

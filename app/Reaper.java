import models.Paste;

import org.joda.time.DateTime;

import play.jobs.Every;
import play.jobs.Job;

@Every("1d")
public class Reaper extends Job
{
    public void doJob ()
    {
        Paste.delete("updated < ?1", DateTime.now().minusDays(7).toDate());
    }
}

"use strict";

window.addEventListener("load", function ()
{
    var debounce, textareas = {}, frame = document.querySelector("iframe");

    // It is easy to generate the requisite multipart/form-data format for upload, but IE9
    // doesn't support XMLHttpRequest.prototype.sendAsBinary making doing so futile.
    if (!("FormData" in window))
    {
        frame = document.querySelector("body noscript");
        frame.outerHTML = '<input type="submit" value="Save">';
        return;
    }

    function save (now)
    {
        var xhr, boundary, area, content;

        xhr = new XMLHttpRequest();
        xhr.open("POST", location.href.replace(/edit\/?#?.*$/, "save"), !now);
        xhr.setRequestHeader("X-Magic", "Wand");

        if (!now)
        {
            xhr.onreadystatechange = function ()
            {
                if (xhr.readyState === 4 && xhr.status === 200)
                {
                    frame.src = location.pathname.replace(/edit\/?#?.*$/, "?" + new Date().getTime());
                }
            };
        }

        xhr.send(new FormData(document.querySelector("form")));
        debounce = null;
    };

    function lines (value, start, end)
    {
        var startLineIndex, endLineIndex, endLineEnd, lines;

        startLineIndex = value.substring(0, start).lastIndexOf("\n") + 1;
        endLineIndex = value.substring(0, end).lastIndexOf("\n") + 1;
        endLineEnd = value.substring(endLineIndex).indexOf("\n");
        lines = value.substring(startLineIndex, endLineEnd === -1 ? value.length : endLineIndex + endLineEnd);

        return {
            start: start - startLineIndex,
            startIndex: startLineIndex,
            end: end - endLineIndex,
            endIndex: endLineIndex,
            lines: lines.split("\n")
        };
    }

    Array.prototype.forEach.call(document.querySelectorAll("form textarea"), function (textarea)
    {
        var tab = textarea.parentNode.firstElementChild.firstElementChild;

        textarea.addEventListener("keydown", function (event)
        {
            var start, end, value, meta;

            start = textarea.selectionStart;
            end = textarea.selectionEnd || start;
            value = textarea.value;
            meta = lines(value, start, end);

            if (event.keyCode === 9)
            {
                event.preventDefault();

                if (event.shiftKey)
                {
                    start = 0;
                    end = meta.endIndex + meta.lines[meta.lines.length - 1].length;

                    textarea.value = meta.lines.reduce(function (previous, line)
                    {
                        var edit = line.replace(/^(?:   \t|  \t| \t|\t|\s{1,4})/, "");
                        start += line.length - edit.length;
                        return previous + edit + "\n";
                    }, value.substring(0, meta.startIndex)) + value.substring(end + 1);

                    if (meta.lines.length > 1)
                    {
                        textarea.setSelectionRange(meta.startIndex, end - start);
                    }
                    else
                    {
                        start = meta.lines[0].replace(/^(\s+).*/, "$1").length - start;
                        textarea.setSelectionRange(meta.startIndex + start, meta.startIndex + start);
                    }
                }
                else if (meta.lines.length > 1)
                {
                    start = 0;
                    end = meta.endIndex + meta.lines[meta.lines.length - 1].length;

                    textarea.value = meta.lines.reduce(function (previous, line)
                    {
                        if (line.length)
                        {
                            start++;
                            return previous + "\t" + line + "\n";
                        }
                        else
                        {
                            return previous + "\n";
                        }
                    }, value.substring(0, meta.startIndex)) + value.substring(end + 1);

                    textarea.setSelectionRange(meta.startIndex, start + end);
                }
                else
                {
                    textarea.value = value.substring(0, start) + "\t" + value.substring(end);
                    textarea.setSelectionRange(start + 1, start + 1);
                }
            }
            else if (event.keyCode === 36)
            {
                event.preventDefault();

                meta = lines(value, start, end);
                start = meta.lines[0].replace(/^(\s*).*/, "$1").length;
                start = meta.startIndex + (meta.start === start ? 0 : start);
                textarea.setSelectionRange(start, event.shiftKey ? meta.endIndex + meta.end : start);
            }
        });

        if (textarea.name === "css" && textarea.value.substring(0, 8) === "/*LESS*/")
        {
            tab.textContent = "LESS";
        }

        textarea.addEventListener("input", function ()
        {
            if (textarea.name === "css")
            {
                tab.textContent = textarea.value.substring(0, 8) === "/*LESS*/" ? "LESS" : "CSS";
            }

            // Autosave. For those "Shit, my browser died!" moments.
            clearTimeout(debounce);
            debounce = setTimeout(save, 5000);
        });

        textareas[textarea.name] = textarea;
    });

    // We autosave. Nix Safari's dialog warning about unsaved text.
    window.addEventListener("beforeunload", function () {});

    window.addEventListener("unload", function ()
    {
        if (debounce)
        {
            clearTimeout(debounce);
            save(true);
        }
    });

    Array.prototype.forEach.call(document.querySelectorAll("form h2"), function (h2)
    {
        h2.addEventListener("click", function ()
        {
            if (debounce)
            {
                clearTimeout(debounce);
                save();
            }
        });
    });
});
